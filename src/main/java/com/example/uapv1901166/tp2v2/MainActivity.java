package com.example.uapv1901166.tp2v2;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ListView listview = findViewById(R.id.listPays);
        BookDbHelper bookDbHelper = new BookDbHelper(this);
        Cursor cursor = bookDbHelper.fetchAllBooks();
        SimpleCursorAdapter simpleCursorAdapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, cursor, new String[] {BookDbHelper.COLUMN_BOOK_TITLE, BookDbHelper.COLUMN_AUTHORS}, new int[] {android.R.id.text1, android.R.id.text2}, 0);
        listview.setAdapter(simpleCursorAdapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Book book = (Book) BookDbHelper.cursorToBook( (Cursor) parent.getItemAtPosition(position));
                Intent intent = new Intent(MainActivity.this, BookActivity.class);
                intent.putExtra("book", book);
                startActivity(intent);
            }
        });
        listview.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
//                menu.add(R.menu.layout);
                getMenuInflater().inflate(R.menu.layout, menu);
                MenuItem supprimer = (MenuItem) findViewById(R.id.item_suppression);
            }
        });
        FloatingActionButton buttonAddBook = (FloatingActionButton) findViewById(R.id.button_ajout);
        buttonAddBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, BookActivity.class);
                intent.putExtra("book", new Book());
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onContextItemSelected(MenuItem menuItem) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo();
        AdapterView adapterView = (AdapterView) findViewById(R.id.listPays);
        Cursor cursor = (Cursor) adapterView.getItemAtPosition(info.position);
        BookDbHelper bookDbHelper = new BookDbHelper(MainActivity.this);
        bookDbHelper.deleteBook(cursor);
        onResume();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        ListView listview = findViewById(R.id.listPays);
        BookDbHelper bookDbHelper = new BookDbHelper(this);
        Cursor cursor = bookDbHelper.fetchAllBooks();
        SimpleCursorAdapter simpleCursorAdapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, cursor, new String[] {BookDbHelper.COLUMN_BOOK_TITLE, BookDbHelper.COLUMN_AUTHORS}, new int[] {android.R.id.text1, android.R.id.text2}, 0);
        listview.setAdapter(simpleCursorAdapter);
    }
}
