package com.example.uapv1901166.tp2v2;

import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BookActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);
        final Book book = getIntent().getParcelableExtra("book");

        final TextView titre = findViewById(R.id.nameBook);
        final TextView auteur = findViewById(R.id.editAuthors);
        final TextView annee = findViewById(R.id.editYear);
        final TextView genre = findViewById(R.id.editGenres);
        final TextView editeur = findViewById(R.id.editPublisher);

        final Button sauvegarde = (Button) findViewById(R.id.button_save);

        final boolean nouveau;

        if (book.getTitle() == null) {
            sauvegarde.setText("Création");
            nouveau = true;
        }
        else {
            titre.setText(book.getTitle());
            auteur.setText(book.getAuthors());
            annee.setText(book.getYear());
            System.out.println(book.getGenres());
            genre.setText(book.getGenres());
            editeur.setText(book.getPublisher());
            nouveau = false;
        }

        sauvegarde.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (titre.getText().toString().contentEquals("") || auteur.getText().toString().contentEquals("") || annee.getText().toString().contentEquals("") || genre.getText().toString().contentEquals("") || editeur.getText().toString().contentEquals("")) {
                    Toast.makeText(BookActivity.this, "Titre vide", Toast.LENGTH_LONG).show();
                    return;
                }
                BookDbHelper bookDbHelper = new BookDbHelper(BookActivity.this);
                book.setTitle(titre.getText().toString());
                book.setAuthors(auteur.getText().toString());
                book.setYear(annee.getText().toString());
                book.setGenres(genre.getText().toString());
                book.setPublisher(editeur.getText().toString());
                if (nouveau) {
                    try {
                        boolean result = bookDbHelper.addBook(book);
                        if (!result) {
                            Toast.makeText(BookActivity.this, "Il existe un livre avec le même titre et le même auteur", Toast.LENGTH_LONG).show();
                        }
                    }
                    catch (SQLiteConstraintException e) {
                        Toast.makeText(BookActivity.this, "Il existe un livre avec le même titre et le même auteur", Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    bookDbHelper.updateBook(book);
                }
                finish();
            }
        });
    }
}
